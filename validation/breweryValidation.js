const Joi = require('joi');
 
const brewerySchema = Joi.object().keys({
    BrewId:Joi.number().integer().min(1).required(),
    BreweryName: Joi.string().min(5).max(30).required(),
    BreweryId:Joi.string().min(5).max(20).required(),
    State : Joi.string().min(5).max(20).required(),
    City: Joi.string().min(5).max(30).required()
});
module.exports=brewerySchema;
