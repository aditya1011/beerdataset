const Joi = require('joi');
 
const beerSchema = Joi.object().keys({
    BeerId:Joi.number().integer().min(1).required(),
    BeerName: Joi.string().min(5).max(30).required(),
    BreweryId:Joi.string().min(5).max(20).required(),
    Style : Joi.string().min(5).max(20).required()
});



module.exports=beerSchema;
