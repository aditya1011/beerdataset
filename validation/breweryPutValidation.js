const Joi = require('joi');
 
const breweryPutSchema = Joi.object().keys({
    // BrewId:Joi.number().integer().min(1),
    BreweryName: Joi.string().min(5).max(30),
    BreweryId:Joi.string().min(5).max(20),
    State : Joi.string().min(5).max(20),
    City: Joi.string().min(5).max(30)
});
module.exports=breweryPutSchema;
