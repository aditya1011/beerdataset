const Joi = require('joi');

const userSchema = Joi.object().keys({
    // id:Joi.number().required(),
    // username:Joi.string().min(5).max(20).required(),
    email:Joi.string().email().required(),
    password:Joi.string().required(),    
});
module.exports=userSchema;
