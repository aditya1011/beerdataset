const Joi = require('joi');
 
const beerPutSchema = Joi.object().keys({
    // BeerId:Joi.number().integer().min(1).required(),
    BeerName: Joi.string().min(5).max(30),
    BreweryId:Joi.string().min(5).max(20),
    Style : Joi.string().min(5).max(20)
})

module.exports=beerPutSchema;
