const client=require('../databaseConnection/db');


function userEmail(id) {
  return client.query('SELECT * FROM users where id= ($1);',[id]);
}

function createNewUser(email,password) {
  return client.query('INSERT INTO users (email,password) VALUES ($1,$2);', [email,password]);
}

module.exports = {createNewUser, userEmail};
