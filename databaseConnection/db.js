const { Client } = require('pg')

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'beer',
    password: 'password',
    port: 5433,
})

client.connect()
.then(() => console.log('connect Successfull'))
.catch((e)=>{
    createLogger.error(`{error:{
    status:500,
    error: ${e.message}}`);
 process.exit();
    })

module.exports = client;