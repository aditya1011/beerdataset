
const beerData = require('./beer.json');
const breweryData = require('./brewery.json');
const client = require('./databaseConnection/db');

// const { Client } = require('pg')
// const client = new Client({
//     user: 'postgres',
//     host: 'localhost',
//     database: 'beer',
//     password: 'password',
//     port: 5433,
// })

function deleteTable(){
let delTable =[];
let delBeer= client.query("Drop table beer");
delTable.push(delBeer);

// let delBeers= client.query("Drop table beers");
// delTable.push(delBeers);

let deluser= client.query("Drop table users");
delTable.push(deluser);

let delBrew = client.query("Drop table brewery");
delTable.push(delBrew);

return Promise.all(delTable);
}

function createTable() {
  let create = [];
    let beerTable = client.query("CREATE TABLE beer(BeerId serial,BeerName VARCHAR (50) ,BreweryId VARCHAR (50),Style VARCHAR (355),primary key(BeerId,BreweryId))");
    create.push(beerTable);
    // beerData.forEach((element) => {
    //     client.query("Insert into beer (BeerId,BeerName,BreweryId,Style) values ($1,$2,$3,$4)", [element.BeerId, element.BeerName, element.BreweryId, element.Style]);
    // })
//     console.log('beer',beerTable);
    let createuser = client.query("CREATE TABLE users(id serial PRIMARY KEY,email varchar(50) NOT NULL UNIQUE,password varchar(500) NOT NULL)");
      create.push(createuser);

   let breweryTable =  client.query("CREATE TABLE brewery(BrewId serial unique not null,BreweryName VARCHAR (50) ,BreweryId VARCHAR (50)primary key,State VARCHAR (35),City varchar(35))");
   create.push(breweryTable); 
   // breweryData.forEach((elems) => {
    //     client.query("insert into brewery (BrewId,BreweryName,BreweryId,State,City) values ($1,$2,$3,$4,$5)", [elems.BrewId, elems.BreweryName, elems.BreweryId, elems.State, elems.City]);
    // })
    
   return Promise.all(create);
}
function insertTable()
{
   let insertQuery=[];
     
      let insertBeer = beerData.forEach((element) => {
            client.query("Insert into beer (BeerId,BeerName,BreweryId,Style) values ($1,$2,$3,$4)", [element.BeerId, element.BeerName, element.BreweryId, element.Style]);
           
        })
        insertQuery.push(insertBeer);


   let insertBrew =  breweryData.forEach((elems) => {
        client.query("insert into brewery (BrewId,BreweryName,BreweryId,State,City) values ($1,$2,$3,$4,$5)", [elems.BrewId, elems.BreweryName, elems.BreweryId, elems.State, elems.City]);
   })
        insertQuery.push(insertBrew); 
           
return Promise.all(insertQuery);

}

// 
    // client.()
    // .then(() => console.log("connect success"))
    // .then(() => deleteTable())
    // .then(() => createTable()) 
    // .then(() => insertTable())  
    // .catch((e) => console.log('error required'+e));
    // console.log(beerData.length);
    deleteTable();
    createTable();
    insertTable();