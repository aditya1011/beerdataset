const express = require('express');
const BeerData = require('../beer.json');
const Joi = require('joi');
const beerSchema = require('../validation/beerValidation');
const beerPutSchema = require('../validation/beerPutValidation');
const beerUtility = require('../database-model/beerdatabase')
const route = express.Router();

function getBeer() {
   //Get all Beer Details.
   route.get('/', (req, res, next) => {
      beerUtility.getBeers().then(() => {
         res.json(BeerData);
      }).catch(next);
   });

   //Get particular details.

   route.get('/:BeerId', (req, res, next) => {
      let BeerId = req.params.BeerId;
      beerUtility.getBeer(BeerId)
         .then(() => {
            var found = BeerData.some((BeerData) => BeerData.BeerId === parseInt(req.params.BeerId))
            console.log(found);
            if (found) {
               res.json(BeerData.filter((Beer) => Beer.BeerId === parseInt(req.params.BeerId)));
            }
            else {
               return res.status(400).json({ msg: 'Not Present in database' });
            }

         }).catch(next);
   });
}


//add new beer.

function postBeer() {

   route.post('/', (req, res) => {
      //   console.log(beerSchema);


      //Validation.
      const result = Joi.validate(req.body, beerSchema)
      console.log(result);
      if (result.error) {
         res.status(400).send(result.error);
      }

      const newBeer = {
         BeerId: req.body.BeerId,
         BeerName: req.body.BeerName,
         BreweryId: req.body.BreweryId,
         Style: req.body.Style
      }

      if (!newBeer.BeerId || !newBeer.BeerName || !newBeer.BreweryId || !newBeer.Style) {
         console.log("not executed");
         return res.status(400).json({ msg: 'please fill all details' });
      }

      BeerData.push(newBeer);
      res.json(BeerData);
   })
}

//modified beerData

function putBeer() {
   route.put('/:BeerId', (req, res) => {
      validate();
      var found = BeerData.some((BeerData) => BeerData.BeerId === parseInt(req.params.BeerId));

      if (found) {
         const updateBeer = req.body;
         // console.log(updateBeer);

         BeerData.forEach((element) => {
            if (element.BeerId === parseInt(req.params.BeerId)) {
               console.log(updateBeer);
               element.BeerId = updateBeer.BeerId ? updateBeer.BeerId : element.BeerId;
               element.BeerName = updateBeer.BeerName ? updateBeer.BeerName : element.BeerName;
               element.BreweryId = updateBeer.BreweryId ? updateBeer.BreweryId : element.BreweryId;
               element.Style = updateBeer.Style ? updateBeer.Style : element.Style;

               res.json({ msg: 'Updatad BeerDetails', BeerData });
            }
         });
      }
      else {
         res.status(400).json({ msg: `not updated on berData${req.params.BeerId}` });
      }
   })
}


function validate() {
   route.put('/:BeerId', (req, res) => {
      var editBeer = BeerData.some((BeerData) => BeerData.BeerId === parseInt(req.params.BeerId));

      if (!editBeer) {
         res.status(404).send("Id is not existing");
      }
      const results = Joi.validate(req.body, beerPutSchema)
      console.log(results);

      if (results.error) {
         res.status(400).send(results.error);
      }

      res.send(editBeer);
   })
}

//Delete beerdetails.

function deleteBeer() {
   route.delete('/:BeerId', (req, res) => {

      var found = BeerData.some((BeerData) => BeerData.BeerId === parseInt(req.params.BeerId));

      if (found) {
         res.json({
            msg: `Deleted BeerId ${req.params.BeerId}`,
            BeerData: BeerData.filter((BeerData) => BeerData.BeerId !== parseInt(req.params.BeerId))
         });
      }
      else {
         res.status(400).json({ msg: `not updated on berData${req.params.BeerId}` });
      }
   })
}

// console.log(BeerData);
getBeer();
postBeer();
putBeer();
deleteBeer();

module.exports = route;