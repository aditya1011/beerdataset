const express = require('express');
const userRouter = express.Router();
const Joi = require('joi');
const bcrypt = require("bcrypt");
const userUtility = require('../database-model/user')
const userSchema = require('../validation/userValidation');

userRouter.post('/signup', (req, res, next) => {
  const newUser = {
      // id:req.body.id,
    // username:req.body.username,
    email: req.body.email,
    password: req.body.password,
  };

  const result = Joi.validate(req.body, userSchema)
  console.log(result);
  if (result.error) {
     res.status(400).send(result.error);
  }

  userUtility.userEmail(newUser.id)
    .then((result) => {
      if (result.rows.length === 0) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err,
            });
          }
          userUtility.createNewUser(newUser.email, hash)
            .then(() => {
              // Created Resource
              res.status(201).json({
                message: 'User Created',
              });
            }).catch(next);
        });
      } else {
        return res.status(400).send('Email Already Exists');
      }
    }).catch(next);
});






// userRouter.post('/signup', (req, res, next) => {

//     bcrypt.hash(req.body.password, 10, (err, hash) => {
//     if (err) {
//             return res.status(500).json({
//                 error: err
//             });
//         }
//  else{
//     const newUser = {
//         id: req.body.id,
//         username: req.body.username,
//         email: req.body.email,
//         password: hash
//     };

//     // const result = Joi.validate(req.body, userSchema)
//     // console.log('res', result);
//     // if (result.error) {
//     //     res.status(400).send(result.error);
//     // }



//     console.log('userEmail', newUser.email);
           
//              userUtility.createNewUser(newUser.id, newUser.username, newUser.email,hash)
//                     .then((result) => {
//                         console.log(result);
//                         res.status(201).json({
//                             message: 'User Created',
//                         });
//                     }).catch(next);

//             userUtility.userEmail(newUser.email)
//                     .then((result) => {
//             //  console.log('useremail',result);
//           }).catch(next);
                  
//         }
//         })
// });

module.exports = userRouter;
