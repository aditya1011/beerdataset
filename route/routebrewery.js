const express = require('express');
const BreweryData = require('../brewery.json');
const Joi = require('joi');
const brewerySchema = require('../validation/breweryValidation');
const breweryPutSchema = require('../validation/breweryPutValidation');
const brewerUtility = require('../database-model/brewerydatabase');
const route = express.Router();

function getBrewery() {

    route.get('/', (req, res, next) => {
        brewerUtility.getBrewers().then(() => {
            res.json(BreweryData);
        }).catch(next);
    });

    route.get('/:BrewId', (req, res, next) => {
        let BrewId = req.params.BrewId;
        brewerUtility.getBrewer(BrewId)
            .then(() => {
                var found = BreweryData.some((BreweryData) => BreweryData.BrewId === parseInt(req.params.BrewId));
                console.log(found);
                if (found) {
                    res.json(BreweryData.filter((Brewery) => Brewery.BrewId === parseInt(req.params.BrewId)));
                }
                else {
                    return res.status(400).json({ msg: 'Not Present in database' });
                }
            }).catch(next);
    })
}

function postBrewery() {
    route.post('/', (req, res) => {
        //  res.send(req.body);
        //  console.log(req.body);

        const result = Joi.validate(req.body, brewerySchema)
        console.log(result);

        if (result.error) {
            res.status(400).send(result.error);
        }
        const newBrewery = {
            BrewId: req.body.BrewId,
            BreweryName: req.body.BreweryName,
            BreweryId: req.body.BreweryId,
            State: req.body.State,
            City: req.body.City
        }

        if (!newBrewery.BrewId || !newBrewery.BreweryName || !newBrewery.BreweryId || !newBrewery.State || !newBrewery.City) {
            console.log("not executed");
            return res.status(400).json({ msg: 'please fill all details' });
        }

        BreweryData.push(newBrewery);
        res.json(BreweryData);
    })
}

//Update 
function putBrewery() {

    route.put('/:BrewId', (req, res) => {
        validate();
        var found = BreweryData.some(BreweryData => BreweryData.BrewId === parseInt(req.params.BrewId));

        if (found) {
            const updateBeer = req.body;

            BreweryData.forEach(element => {
                if (element.BrewId === parseInt(req.params.BrewId)) {
                    console.log(updateBeer);
                    element.BrewId = updateBeer.BrewId ? updateBeer.BrewId : element.BrewId;
                    element.BreweryName = updateBeer.BreweryName ? updateBeer.BreweryName : element.BreweryName;
                    element.BreweryId = updateBeer.BreweryId ? updateBeer.BreweryId : element.BreweryId;
                    element.State = updateBeer.State ? updateBeer.State : element.State;
                    element.City = updateBeer.City ? updateBeer.City : element.City;

                    res.json({ msg: 'Updatad BeerDetails', BreweryData });
                }
            });
        }
        else {
            res.status(400).json({ msg: `not updated on berData${req.params.BrewId}` });
        }
    })
}

function validate() {
    route.put('/:BrewId', (req, res) => {
        var editBrew = BreweryData.some(BreweryData => BreweryData.BrewId === parseInt(req.params.BrewId));

        if (!editBrew) {
            res.status(404).send("Id is not existing");
        }
        const results = Joi.validate(req.body, breweryPutSchema)
        console.log(results);

        if (results.error) {
            res.status(400).send(results.error);
        }

        res.send(editBrew);
    })
}

function deleteBrewery() {
    route.delete('/:BrewId', (req, res) => {

        var found = BreweryData.some(BreweryData => BreweryData.BrewId === parseInt(req.params.BrewId));

        if (found) {
            const updateBrew = req.body;

            res.json({
                msg: `Deleted BrewId ${req.params.BrewId}`,
                BreweryData: BreweryData.filter(BreweryData => BreweryData.BrewId !== parseInt(req.params.BrewId))
            });
        }
        else {
            res.status(400).json({ msg: `not updated on berData${req.params.BrewId}` });
        }
    })
}
// console.log(BreweryData);
getBrewery();
postBrewery();
putBrewery();
deleteBrewery();
module.exports = route;