// const moment = require('moment');

// const logger = (req, res, next) => {
//     console.log(
//         `${req.protocol}://${req.get('host')}${
//         req.originalUrl}:${moment().format()}`
//     );
//     next();
// }


const {createLogger,format,transports} = require('winston');

module.exports = createLogger ({
    format: format.combine(format.simple(),
        format.timestamp(),
        format.printf((info) => `[${info.timestamp}]${info.level}${info.message}`)
    ),
    transports :[
        new transports.File ({
            maxsize : 512000,
            maxFiles : 5,
            filename: `${__dirname}/../logs/log-api.log` 
        }),
        new transports.Console ({ 
            level : 'debug'            
        })
    ]
})

// module.exports = logger;