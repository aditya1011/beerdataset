const express = require('express');
const logger = require('./logger/logger ');
const app = express();

// middleware
const logg = (req, res, next) => {
    logger.info(
        `${req.protocol}://${req.get('host')}
         ${req.originalUrl}${req.method}`
    );
    next();
}
app.use(logg);

//get body parser.
app.use(express.json());
//app.use(express.urlencoded({extended:false }));
//API for beerDetails.
app.use('/api/BeerData', require('./route/routebeer'));
//API for breweryDetails
app.use('/api/BreweryData', require('./route/routebrewery'));

app.use('/api/user',require('./route/user'))

//Error-handling middleware.
// app.use('./route');
app.use((error, req, res, next) => {
    res.status( 500).json({
      error: {
        message: error.message
      }
    });
})
    

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => logger.info("Server is start on 5000"));

